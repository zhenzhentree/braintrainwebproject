

# Start the app with docker-compose 

## Steps 


**1. clone the project**

download project from website or:
```bash
git clone git@gitlab.com:zhenzhentree/braintrainwebproject.git
#or:
git clone https://gitlab.com/zhenzhentree/braintrainwebproject.git

```


**2. start couchdb and the app with docker-compose**
after switched into the project folder:
```bash
docker-compose build
docker-compose up

```
**3. create tables in couchdb**

```bash
HOST="http://tree:1234@127.0.0.1:5984"

curl -X PUT $HOST/user
curl -X PUT $HOST/karte
curl -X PUT $HOST/kasten
curl -X PUT $HOST/user_learning_history
```
**4. open http://localhost:8080**

play with the BrainTrain app in browser
(PS: due to the recent dockercompose update some frondend rendering is missing
fixing comming up soon)

**5. stop/remove couchdb and the app with docker-compose**
```bash
#stop/remove
docker-compose down
#final delete of the complete database and data in its volunm
docker-compose down -v
 ```


.



