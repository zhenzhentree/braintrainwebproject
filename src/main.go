package main

import (
	"app/controller"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
)

func main() {

	R := mux.NewRouter()

	R.PathPrefix("/static/").Handler(http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))))

	R.HandleFunc("/", controller.Index)
	R.HandleFunc("/users", controller.Registration).Methods("GET", "POST")          //registration
	R.HandleFunc("/authenticate-user", controller.AuthenticateUser).Methods("POST") //login

	R.HandleFunc("/kaesten", controller.Karteikaesten).Name("karteikaesten").Methods("GET", "POST", "DELETE") //karteikästen

	R.HandleFunc("/users/kaesten/anschauen/{kastenid:[0-9a-z]+}/{karteid:[0-9a-z]+}", controller.Karteikaesten_anschauen) //karte id in original case set to 0

	R.HandleFunc("/logout", controller.Logout)

	R.HandleFunc("/users/meinekarteien/{kastenid:[0-9a-z]+}", controller.Auth(controller.MeineKarteikästen)) //wenn es nicht um eine bestimmte kastenid geht, wird em ende 0 angehängen

	R.HandleFunc("/users/kaesten", controller.Auth(controller.Kasten_erstellen)).Methods("POST", "GET")                                                           //kasten_erstellen
	R.HandleFunc("/users/kaesten/bearbeiten/{kastenid:[0-9a-z]+}", controller.Auth(controller.Kasten_bearbeiten)).Methods("POST", "GET")                          //kasten_bearbeiten/users/kaesten/bearbeiten/
	R.HandleFunc("/users/kaesten/{kastenid:[0-9a-z]+}", controller.Auth(controller.Karte_erstellen)).Methods("GET", "POST")                                       //karte_erstellen
	R.HandleFunc("/users/kaesten/{kastenid:[0-9a-z]+}/karte/bearbeiten/{karteid:[0-9a-z]+}", controller.Auth(controller.Karte_bearbeiten)).Methods("GET", "POST") //karte bearbeiten
	R.HandleFunc("/users/kaesten/detele/{kastenid:[0-9a-z]+}", controller.Auth(controller.Kasten_Delete)).Methods("DELETE", "GET")                                //kasten delete

	R.HandleFunc("/users/kaesten/{kastenid:[0-9a-z]+}/karten/delete/{karteid:[0-9a-z]+}", controller.Auth(controller.Delete_Karte))

	R.HandleFunc("/users/kaesten/{kastenid:[0-9a-z]+}/lernhistories", controller.Auth(controller.Lernen))

	R.HandleFunc("/users/profil", controller.Auth(controller.MeinProfil)).Methods("GET", "DELETE", "POST") //meinProfil ///users/{userid:[0-9a-z]+}

	//api beispiel ohne authentifizierung.
	R.HandleFunc("/api/users/kaesten", controller.Auth(controller.GetUserKaestenApi)).Methods("POST", "GET")

	log.Fatal(http.ListenAndServe("app:8080", R))

}
