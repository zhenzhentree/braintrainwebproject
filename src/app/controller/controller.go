package controller

import (
	"app/model"
	"html/template"
	"net/http"
	"regexp"
	"strconv"
)

//func zu increment
var funcMap = template.FuncMap{
	"inc": func(i int) int { return i + 1 },
}

type entry struct {
	Icon   string
	Name   string
	Href   string
	Number string
	Hover  string
}
type Obercategory struct {
	Name           string
	ElementKaesten []ElementKasten
}
type ElementKasten struct {
	Element            model.Kasten
	ElementKartenCount int
}
type MeineKaestenElement struct {
	Element            model.Kasten
	ElementKartenCount int
	ElementLevel       string
}
type PageParameter struct {
	PageTitle                   string
	Menuentry                   []entry
	Login                       bool
	Karten                      []model.Karte //add karten
	KartenCountByID             int           //lernen //bearbeiten
	Obercategorys               []Obercategory
	MeineKaesten                []ElementKasten       //meineKarteikästen
	MeineKaesten2               []MeineKaestenElement //meineKarteikästen
	CurrentKasten               model.Kasten
	LernenKarte                 model.Karte
	CurrentUser                 model.User
	AnschauenKarten             []model.Karte
	CurrentKarte                model.Karte           //edit karte
	KastenVonAnderen            []ElementKasten       //meineKarteikästen
	KastenVonAnderen2           []MeineKaestenElement //meineKarteikästen
	KartenFachAnzahl            [5]int
	UserCount                   int //index
	AllKartenCount              int //index
	AllKaestenCount             int //index
	NameFalse                   bool
	PswFalse                    bool
	EmailFalse                  bool
	CheckBoxFalse               bool
	CategoryFalse               bool
	KastenErstllenFalse         bool
	PswGleich                   bool
	PswUngleich                 bool
	KastenCountByUserID         int //profil
	KartenCountByUserID         int
	KarteErstllenFalse          bool
	Error                       bool
	ErstelltVon                 string //lernen
	Mime                        string
	ProfilBildName              string
	KaestenCountByUserId        int
	VonAnderenUser              bool
	FortschrittLernHistory      string
	KartenFachAnzahlLernHistory [5]int
	Fortschritt                 string //bei original user
	LernLevel                   string //kartelevel
}

var tmpl *template.Template
var data PageParameter

var Meinekastencount string //used in struct entry for the side menu
var Obercategories = [5]string{"Naturwissenschaften", "Sprachen", "Gesellschaft", "Wirtschaft", "Geisteswissenschaften"}
var rKarteikaesten = regexp.MustCompile(`meineKarteikästen`) // Contains "Karteikaesten"

// Is executed automatically on package load
func init() {

	countkasten := model.GetPublicKastenCount()
	countstring := strconv.Itoa(countkasten)

	data = PageParameter{
		PageTitle:     "Page1",
		Login:         true,
		Error:         false,
		CategoryFalse: false,
		Menuentry: []entry{
			{Icon: "/static/Icons/Home.svg", Name: "Home", Href: "/"},
			{Icon: "/static/Icons/Karteikasten.svg", Name: "Karteikästen", Href: "/kaesten", Number: countstring},
		},
	}

}

// Index controller
func Index(w http.ResponseWriter, r *http.Request) {

	if rKarteikaesten.MatchString(r.URL.Path) {
		MeineKarteikästen(w, r)
		return
	}

	data.UserCount = model.GetUserCount()
	_, data.AllKartenCount = model.GetAllKartenUndKartenCount()
	data.AllKaestenCount = model.GetAllKaestenCount()
	temp := data

	temp.Menuentry[0].Hover = "background-color: #34495E"
	if temp.Login == false {
		temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})
	}
	tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/startseite.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", temp)
}
