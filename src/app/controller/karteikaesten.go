package controller

import (
	"app/model"
	"encoding/json"
	"html/template"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Karteikaesten(w http.ResponseWriter, r *http.Request) {

	temp := data
	session, _ := store.Get(r, "session")
	temp.PageTitle = "Karteikaesten"

	if r.Method == "GET" {

		var elementKaesten [5][]ElementKasten
		for j := 0; j < 5; j++ {
			kaesten := model.ReturnKastenByOberCate(Obercategories[j])
			elementKaesten[j] = ElementKaestenHelper(kaesten)

		}

		//Obercategory kann nicht append weil der typ kein struct ist
		temp.Obercategorys =
			[]Obercategory{
				{Name: "Naturwissenschaften", ElementKaesten: elementKaesten[0]},
				{Name: "Sprachen", ElementKaesten: elementKaesten[1]},
				{Name: "Gesellschaft", ElementKaesten: elementKaesten[2]},
				{Name: "Wirtschaft", ElementKaesten: elementKaesten[3]},
				{Name: "Geisteswissenschaft", ElementKaesten: elementKaesten[4]},
			}
	} else {
		//post
		category := r.FormValue("category")
		obercategory := r.Form.Get("hidden")

		//todo--  sonstig category kann nicht richtig ausgefoltert werden muss mit obercategory zusammenn filter
		kaesten := model.Filter(category)

		elementKaesten := ElementKaestenHelper(kaesten)

		temp.Obercategorys = []Obercategory{{Name: obercategory, ElementKaesten: elementKaesten}}
	}
	//falls der user eingelogged ist, hat er die Möglichkeit auch zu anderen links wie meinekarteikasten oder Profil zu gehen
	if session.Values["userid"] != "" && session.Values["userid"] != nil {

		user_id := session.Values["userid"].(string)
		temp.CurrentUser = model.GetUserById(user_id)
		Meinekastencount = strconv.Itoa(model.GetMeineKastencount(user_id))

		temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})
	}

	temp.Menuentry[0].Hover = ""
	temp.Menuentry[1].Hover = "background-color: #34495E"
	tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/karteikaesten.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", temp)
}

func MeineKarteikästen(w http.ResponseWriter, r *http.Request) {
	// to "post" the delete case
	temp := data
	methode := r.FormValue("_methode")

	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)
	kaesten, _ := model.GetKaestenByUserId(userid)
	temp.MeineKaesten2 = MeineKaestenElementHelper(kaesten, w, r)
	temp.KastenVonAnderen2 = MeineKaestenElementHelper(model.GetUnrepeatedKastenVonAnderenByUserId(userid), w, r)
	if methode == "DELETE" {

		Kasten_Delete(w, r)
		kaesten, _ = model.GetKaestenByUserId(userid)
		temp.MeineKaesten2 = MeineKaestenElementHelper(kaesten, w, r)

		temp.KastenVonAnderen2 = MeineKaestenElementHelper(model.GetUnrepeatedKastenVonAnderenByUserId(userid), w, r)

	} else if r.Method == "POST" { // filter durch http post

		category := r.FormValue("category")
		//obercategory := r.Form.Get("hidden")
		//	println(obercategory) //fehlt es jetzt
		//todo--  sonstig category kann nicht richtig ausgefoltert werden muss mit obercategory zusammenn filter
		kaesten := model.FilterPrivateKaesten(userid, category)

		elementKaesten := MeineKaestenElementHelper(kaesten, w, r)
		temp.MeineKaesten2 = elementKaesten

		anderenkaesten := model.FilterKaestenVonAnderen(userid, category)
		temp.KastenVonAnderen2 = MeineKaestenElementHelper(anderenkaesten, w, r)

	}

	temp.PageTitle = "MeineKarteikästen"
	temp.Menuentry[1].Number = strconv.Itoa(model.GetPublicKastenCount())
	Meinekastencount = strconv.Itoa(model.GetMeineKastencount(userid))

	temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})
	temp.Menuentry[2].Hover = "background-color: #34495E"
	temp.Menuentry[0].Hover = ""
	tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/meineKarteikästen.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", temp)

}

func Kasten_bearbeiten(w http.ResponseWriter, r *http.Request) {
	temp := data

	temp.KastenErstllenFalse = true // Kasten nur editen

	if r.Method == "GET" {
		session, _ := store.Get(r, "session")
		userid := session.Values["userid"].(string)
		vars := mux.Vars(r)
		kastenid := vars["kastenid"]
		kasten, _ := model.GetKastenByKastenId(kastenid)
		temp.CurrentKasten = kasten
		temp.CurrentUser = model.GetUserById(userid)
		temp.KartenCountByID = model.GetKartenCountById(kastenid)
		Meinekastencount = strconv.Itoa(model.GetMeineKastencount(userid))
		temp.CategoryFalse = false

		temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})

		tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten.tmpl", "template/login.tmpl")
		tmpl.ExecuteTemplate(w, "layout", temp)
	} else if r.Method == "POST" {
		Update_Kasten(w, r)
	}

}
func Kasten_Delete(w http.ResponseWriter, r *http.Request) {

	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)

	vars := mux.Vars(r)
	kastenid := vars["kastenid"]
	kasten, _ := model.GetKastenByKastenId(kastenid)
	if len(userid) == 0 {
		println("Error NO User ID GIVEN ABORTING")
		http.Error(w, "No User id given", 404)
		return
	}
	kasten.DeleteKasten()

}
func Update_Kasten(w http.ResponseWriter, r *http.Request) {

	temp := data
	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)

	vars := mux.Vars(r)
	kastenid := vars["kastenid"]
	kasten, _ := model.GetKastenByKastenId(kastenid)

	if len(userid) == 0 {
		println("Error NO User ID GIVEN ABORTING")
		http.Error(w, "No User id given", 404)
		return
	}
	obercategory := ""
	category := ""

	title := r.FormValue("title")
	category = r.FormValue("category")
	beschreibung := r.FormValue("beschreibung")
	sichtbarkeit := r.Form.Get("sichtbarkeit")
	obercategory = r.Form.Get("hidden")

	if category != "" && obercategory != "" { //etwas ausgewählt

		temp.CategoryFalse = false
		kasten.Title = title
		kasten.Category = category
		kasten.Obercategory = obercategory
		kasten.Sichtbarkeit = sichtbarkeit
		kasten.Beschreibung = beschreibung

		kasten.UpdateKasten()

		http.Redirect(w, r, "/users/kaesten/"+kastenid, 302)

	} else {

		temp.CategoryFalse = true

		tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten.tmpl", "template/login.tmpl")
		tmpl.ExecuteTemplate(w, "layout", temp)

	}

}

func Kasten_erstellen(w http.ResponseWriter, r *http.Request) {
	temp := data

	temp.KastenErstllenFalse = false //neues Kasten erstellen
	if r.Method == "GET" {
		session, _ := store.Get(r, "session")
		userid := session.Values["userid"].(string)
		temp.CurrentUser = model.GetUserById(userid)
		Meinekastencount = strconv.Itoa(model.GetMeineKastencount(userid))

		temp.CategoryFalse = false

		temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})
		tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten.tmpl", "template/login.tmpl")
		tmpl.ExecuteTemplate(w, "layout", temp)
	} else if r.Method == "POST" {
		Add_Kasten(w, r)
	}

}

func GetUserKaestenApi(w http.ResponseWriter, r *http.Request) {

	temp := data

	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)
	kaesten, _ := model.GetKaestenByUserId(userid)
	temp.MeineKaesten2 = MeineKaestenElementHelper(kaesten, w, r)
	temp.KastenVonAnderen2 = MeineKaestenElementHelper(model.GetUnrepeatedKastenVonAnderenByUserId(userid), w, r)

	b, _ := json.Marshal(temp.MeineKaesten2)
	w.Write(b)

}

func Add_Kasten(w http.ResponseWriter, r *http.Request) {

	temp := data

	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)

	if len(userid) == 0 {
		println("Error NO User ID GIVEN ABORTING")
		http.Error(w, "No User id given", 404)
		return
	}

	var kastenID string
	title := r.FormValue("title")
	category := r.FormValue("category")
	beschreibung := r.FormValue("beschreibung")
	sichtbarkeit := r.Form.Get("sichtbarkeit")
	obercategory := r.Form.Get("hidden")

	var err error
	if len(title) != 0 && len(sichtbarkeit) != 0 {

		if category != "eine Kategorie auswählen" { //etwas ausgewählt
			k := model.Kasten{
				Type:         "Kasten",
				Obercategory: obercategory,
				Category:     category,
				Beschreibung: beschreibung,
				Sichtbarkeit: sichtbarkeit,
				Title:        title,
				Userid:       userid,
				Level:        "0",
			}
			kastenID, err = k.AddKasten()

			temp.KartenCountByID = model.GetKartenCountById(kastenID)
			http.Redirect(w, r, "/users/kaesten/"+kastenID, 302)

		} else {
			temp.CategoryFalse = true
		}
		if err == nil && sichtbarkeit == "öffentlich" {
			temp.Menuentry[1].Number = strconv.Itoa(model.GetPublicKastenCount())
		}
	}

	tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", temp)
}
func ElementKaestenHelper(kaesten []model.Kasten) []ElementKasten {

	var elementKaesten []ElementKasten

	for i := 0; i < len(kaesten); i++ {
		kastenid := kaesten[i].Id
		count := model.GetKartenCountById(kastenid)
		elementKaesten = append(elementKaesten, ElementKasten{Element: kaesten[i], ElementKartenCount: count})
	}

	return elementKaesten

}
func MeineKaestenElementHelper(kaesten []model.Kasten, w http.ResponseWriter, r *http.Request) []MeineKaestenElement {
	var elementKaesten []MeineKaestenElement
	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)

	for i := 0; i < len(kaesten); i++ {
		kastenid := kaesten[i].Id
		count := model.GetKartenCountById(kastenid)
		lns := model.GetLernHistorysByUserKastenId(userid, kastenid)
		level := "0"
		if len(lns) != 0 {
			level = lns[0].Kastenlevel
		}
		elementKaesten = append(elementKaesten, MeineKaestenElement{Element: kaesten[i], ElementKartenCount: count, ElementLevel: level})
	}

	return elementKaesten
}

func Karteikaesten_anschauen(w http.ResponseWriter, r *http.Request) {
	temp := data
	userid := ""
	session, _ := store.Get(r, "session")
	logged := session.Values["userid"] != "" && session.Values["userid"] != nil
	if logged {

		userid = session.Values["userid"].(string)
		temp.CurrentUser = model.GetUserById(userid)
		Meinekastencount = strconv.Itoa(model.GetMeineKastencount(userid))
	}

	vars := mux.Vars(r)
	kastenid := vars["kastenid"]
	currentkasten, _ := model.GetKastenByKastenId(kastenid)
	temp.CurrentKasten = currentkasten
	temp.AnschauenKarten, _ = model.GetAllKartenByKastenID(kastenid)

	karten, _ := model.GetAllKartenByKastenID(kastenid)

	karteid := vars["karteid"]
	if len(karten) == 0 {
		//für ein Kasten  gibt es noch keine Karten

	} else if karteid == "" {
		temp.CurrentKarte = karten[0]
	} else {
		temp.CurrentKarte, _ = model.GetKarte(karteid)
	}

	temp.KartenCountByID = model.GetKartenCountById(kastenid)
	temp.ErstelltVon = model.GetUserById(currentkasten.Userid).Nutzername

	if logged {
		temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})
	}
	tmpl, _ = template.New("test").Funcs(funcMap).ParseFiles("template/layout.tmpl", "template/karteikaesten_anschauen.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", temp)
}
