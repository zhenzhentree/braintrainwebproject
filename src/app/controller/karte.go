package controller

import (
	"app/model"
	"html/template"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Update_Karte(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)
	karteid := vars["karteid"]
	kastenid := vars["kastenid"]
	if len(karteid) == 0 {
		println("Error NO KASTEN ID GIVEN ABORTING")
		http.Error(w, "No Kasten id given", 404)
		return
	}

	karte, _ := model.GetKarte(karteid) //selected arte
	kasten, _ := model.GetKastenByKastenId(kastenid)

	title := r.FormValue("title")
	frage := r.FormValue("frage")
	antwort := r.FormValue("antwort")

	if len(title) != 0 && len(frage) != 0 && len(antwort) != 0 {
		k := model.Karte{
			Id:       karte.Id,
			Rev:      karte.Rev,
			Type:     "karte",
			Level:    "0",
			Frage:    frage,
			Antwort:  antwort,
			Title:    title,
			Kastenid: kastenid,
		}

		k.UpdateKarte()

		karten, _ := model.GetAllKartenByKastenID(kastenid)
		data.Karten = karten

		data.CurrentKasten = kasten
		http.Redirect(w, r, "/users/kaesten/"+kastenid, 302) //nach dem update direekt zur Seite karte-erstelln gehen damit der user überhaupt neue karte erstellen könnte!

	}

	//falls frage || antwort ||title =="", render the same page for edit
	temp := data
	temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})
	tmpl, _ = template.New("").Funcs(funcMap).ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten2.tmpl", "template/login.tmpl")

	tmpl.ExecuteTemplate(w, "layout", temp)
}

func Karte_bearbeiten(w http.ResponseWriter, r *http.Request) { //nur karte bearbeiten

	vars := mux.Vars(r)
	kastenid := vars["kastenid"]
	karteid := vars["karteid"]
	data.KarteErstllenFalse = true
	data.KartenCountByID = model.GetKartenCountById(kastenid)
	if r.Method == "GET" {
		temp := data

		karte, _ := model.GetKarte(karteid)
		temp.CurrentKarte = karte
		karten, _ := model.GetAllKartenByKastenID(kastenid)
		temp.Karten = karten
		tmpl, _ = template.New("test").Funcs(funcMap).ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten2.tmpl", "template/login.tmpl")

		tmpl.ExecuteTemplate(w, "layout", temp)
	} else {

		Update_Karte(w, r)

	}

}

func karteInLernHistorySpeichern(userid string, kastenid string) {
	karten, _ := model.GetAllKartenByKastenID(kastenid)
	for i := 0; i < len(karten); i++ {
		ln := model.User_leraning_history{
			Kastenid:    kastenid,
			Userid:      userid,
			Karteid:     karten[i].Id,
			Kartelevel:  "0",
			Kastenlevel: "0",
		}
		ln.AddLernen()
	}

}

func Karte_erstellen(w http.ResponseWriter, r *http.Request) { //nur karte erstellennicht bearbeiten

	data.KarteErstllenFalse = false
	vars := mux.Vars(r)
	kastenid := vars["kastenid"]
	data.KartenCountByID = model.GetKartenCountById(kastenid)
	if r.Method == "GET" {
		temp := data

		session, _ := store.Get(r, "session")
		userid := session.Values["userid"].(string)

		karten, _ := model.GetAllKartenByKastenID(kastenid)
		temp.Karten = karten
		temp.CurrentKasten, _ = model.GetKastenByKastenId(kastenid)

		temp.CurrentUser = model.GetUserById(userid)
		Meinekastencount = strconv.Itoa(model.GetMeineKastencount(userid))

		temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})

		tmpl, _ = template.New("test").Funcs(funcMap).ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten2.tmpl", "template/login.tmpl")

		tmpl.ExecuteTemplate(w, "layout", temp)
	} else {
		Add_Karte(w, r)
	}

}

//addKarte_controller //POST handler für "bearbeiten_erstellen_2"
func Add_Karte(w http.ResponseWriter, r *http.Request) {
	//data.Login = false //not render "login button. schon logged in "

	temp := data
	vars := mux.Vars(r)
	kastenid := vars["kastenid"]
	if len(kastenid) == 0 {
		println("Error NO KASTEN ID GIVEN ABORTING")
		http.Error(w, "No Kasten id given", 404)
		return
	}

	kasten, _ := model.GetKastenByKastenId(kastenid)

	title := r.FormValue("title")
	frage := r.FormValue("frage")
	antwort := r.FormValue("antwort")

	if len(title) != 0 && len(frage) != 0 && len(antwort) != 0 {
		k := model.Karte{
			Type:     "karte",
			Level:    "0",
			Frage:    frage,
			Antwort:  antwort,
			Title:    title,
			Kastenid: kastenid,
		}
		k.AddKarte()
	}
	karten, _ := model.GetAllKartenByKastenID(kastenid)
	temp.Karten = karten
	//nachdem karte adden muss kasten(level) aktualisiert werden
	kastenlevel := strconv.Itoa(model.FortschrittBerechnen(kastenid))
	kasten.Level = kastenlevel
	kasten.UpdateKasten()
	temp.CurrentKasten = kasten //nach dem Kasten update data variablen muss erneuert übergeben
	temp.KartenCountByID = model.GetKartenCountById(kastenid)

	temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})
	tmpl, _ = template.New("").Funcs(funcMap).ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten2.tmpl", "template/login.tmpl")

	tmpl.ExecuteTemplate(w, "layout", temp)

}
func Delete_Karte(w http.ResponseWriter, r *http.Request) {
	//data.Login = false //not render "login button. schon logged in "
	temp := data
	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)
	if len(userid) == 0 {
		println("Error NO KASTEN ID GIVEN ABORTING")
		http.Error(w, "No User id given", 404)
		return
	}

	vars := mux.Vars(r)
	karteid := vars["karteid"]
	kastenid := vars["kastenid"]
	karte, _ := model.GetKarte(karteid)

	karte.DeleteKarte()
	kasten, _ := model.GetKastenByKastenId(kastenid)
	//nachdem karte deleten muss kasten(level) aktualisiert werden
	kastenlevel := strconv.Itoa(model.FortschrittBerechnen(kastenid))
	kasten.Level = kastenlevel
	kasten.UpdateKasten()

	karten, _ := model.GetAllKartenByKastenID(kastenid)
	temp.Karten = karten
	temp.CurrentKasten = kasten
	temp.CurrentUser = model.GetUserById(userid)
	temp.CurrentKarte = karte
	temp.KarteErstllenFalse = false
	Meinekastencount = strconv.Itoa(model.GetMeineKastencount(userid))

	temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})

	tmpl, _ = template.New("test").Funcs(funcMap).ParseFiles("template/layout.tmpl", "template/Karteikarten_erstellen_bearbeiten2.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", temp)

}
