package controller

import (
	"app/model"
	"html/template"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

func Lernen(w http.ResponseWriter, r *http.Request) { //lernen nur für eingelogten user
	temp := data
	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)
	richtig_id := r.FormValue("richtig") //karteid
	wrong_id := r.FormValue("wrong")     //karteid
	vars := mux.Vars(r)
	kastenid := vars["kastenid"]

	kasten, _ := model.GetKastenByKastenId(kastenid)
	progress := ""
	kartelevel := ""
	//wenn user zum ersten mal iauf der seite lernen landen, sofort alle karten eines Kasten in LernHistory speichern
	if len(model.GetLernHistorysByUserKastenId(userid, kastenid)) == 0 {
		karteInLernHistorySpeichern(userid, kastenid)
		progress = "0"
	} else {
		progress = model.GetLernHistorysByUserKastenId(userid, kastenid)[0].Kastenlevel
	}

	fertigGelernt := false
	if richtig_id != "" {

		fertigGelernt, progress, kartelevel = model.LevelUp(userid, kastenid, richtig_id)

	}
	if wrong_id != "" {

		progress = model.LevelDown(userid, kastenid, wrong_id)
		kartelevel = "0"
	}
	//ramdon karte generieren
	kart := model.NaechsteKarte(userid, kastenid)
	allGelernt := model.AlleKartenDerKarteiGelernt(userid, kastenid)
	//falls die karte ist fertiggerlert, sollte sie nicht mehr auftauchen
	if !allGelernt {
		if fertigGelernt {
			for richtig_id == kart.Id {
				kart = model.NaechsteKarte(userid, kastenid)
			}
		}
	}
	//data Daten zuweisen
	for i := 0; i < 5; i++ {
		temp.KartenFachAnzahl[i], _ = model.GetCountUndLernHistoryByFach(userid, kastenid, i)
	}

	temp.Fortschritt = progress
	temp.LernenKarte = kart
	if kartelevel != "" {
		temp.LernLevel = kartelevel
	} else {
		temp.LernLevel = model.GetLernHistorysByExactIds(userid, kastenid, kart.Id).Kartelevel
	}
	temp.CurrentUser = model.GetUserById(userid)
	Meinekastencount = strconv.Itoa(model.GetMeineKastencount(userid))
	temp.KartenCountByID = model.GetKartenCountById(kastenid)
	temp.CurrentKasten = kasten

	temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})

	tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/lernen.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", temp)
}
