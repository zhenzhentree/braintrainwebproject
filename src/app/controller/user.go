package controller

import (
	"app/model"
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"html/template"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/sessions"
	"github.com/twinj/uuid"
	"golang.org/x/crypto/bcrypt"
)

var store *sessions.CookieStore

func init() {
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key := make([]byte, 32)
	rand.Read(key)
	store = sessions.NewCookieStore(key)
}

// Register controller
func Registration(w http.ResponseWriter, r *http.Request) {
	temp := data

	if r.Method == "GET" {

		temp.PageTitle = "registration"
		temp.NameFalse = false
		temp.EmailFalse = false
		temp.PswFalse = false
		temp.CheckBoxFalse = false
		//temp.Login = true
		tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/registration.tmpl", "template/login.tmpl")
		tmpl.ExecuteTemplate(w, "layout", temp)
	} else if r.Method == "POST" {
		Add_User(w, r)
	}
}

//https://tutorialedge.net/golang/go-file-upload-tutorial/?fbclid=IwAR2rDcngTUy1zDLxAofFJjuz2NF4ixN1JGd2VRJr_GUggvCs_AJUNi41AlM
func UploadProfilBild(w http.ResponseWriter, r *http.Request) string {

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `editbild`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("editbild")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return ""
	}
	defer file.Close()
	//fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	//fmt.Printf("File Size: %+v\n", handler.Size)
	//fmt.Printf("MIME Header: %+v\n", handler.Header)

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	//get the mime
	//mime := mime.TypeByExtension(filepath.Ext(handler.Filename))
	//println(mime)

	uid := uuid.NewV4()
	imageName := uid.String() + "." + (strings.Split(handler.Filename, "."))[1]
	// Create a file within our image directory
	err = ioutil.WriteFile("./static/Images/"+imageName, fileBytes, 0644) //this path ist file path diferent from the paths in templates
	if err != nil {
		fmt.Println(err)
	}
	return imageName
}
func MeinProfil(w http.ResponseWriter, r *http.Request) {
	temp := data
	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)
	temp.PageTitle = "MeinProfil"
	temp.ProfilBildName = model.GetProfilBildName(userid)

	if r.Method == "POST" {
		Update_User(w, r)
	}
	if "delete" == r.FormValue("delete") { //delete profil

		DeleteUserProfil(w, r)
		temp.Menuentry[1].Number = strconv.Itoa(model.GetPublicKastenCount())

		http.Redirect(w, r, "/logout", http.StatusFound)
	}

	if r.FormValue("upload") == "upload" {
		image := UploadProfilBild(w, r)
		user := model.GetUserById(userid)
		user.ProfilBildName = image
		user.UpdateProfilbild()

		temp.ProfilBildName = image

	}

	temp.PswGleich = true
	temp.PswUngleich = true
	temp.PswFalse = false
	temp.EmailFalse = false

	temp.KartenCountByUserID = model.GetKartenCountByUserID(userid)
	temp.CurrentUser = model.GetUserById(userid)
	meinkastenCount := model.GetMeineKastencount(userid)
	Meinekastencount = strconv.Itoa(meinkastenCount)
	temp.KaestenCountByUserId = meinkastenCount

	temp.Menuentry = append(temp.Menuentry, entry{Icon: "/static/Icons/Meine-Karteien.svg", Name: "Meine Karteikästen", Href: "/users/meinekarteien/0", Number: Meinekastencount}, entry{Icon: "/static/Icons/Mein-Profil.svg", Name: "Mein Profil", Href: "/users/profil", Number: ""})
	temp.Menuentry[3].Hover = "background-color: #34495E"
	temp.Menuentry[0].Hover = ""
	tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/meinProfil.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", temp)

}

func Update_User(w http.ResponseWriter, r *http.Request) {

	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)
	user := model.GetUserById(userid)
	rev := user.Rev
	username := user.Nutzername
	email := r.FormValue("email")
	passwortAlt := r.FormValue("passwortAlt")
	passwortNeu := r.FormValue("passwortNeu")
	passwortNeu2 := r.FormValue("passwortNeu2")

	emailbool := model.CheckEmail(email)
	pswbool := model.CheckPasswort(userid, passwortAlt)
	pswGleich := strings.Compare(passwortNeu, passwortNeu2)  // sollte gleich sein
	pswUngleich := strings.Compare(passwortNeu, passwortAlt) //sollte ungleich sein
	ok := true
	if len(username) != 0 && len(email) != 0 && len(passwortNeu) != 0 {
		if !emailbool {
			data.EmailFalse = true
			ok = false

		}
		if !pswbool {
			data.PswFalse = true
			ok = false

		}
		if pswGleich != 0 { // == 0 ist nicht dann false
			data.PswGleich = false //not gleich
			ok = false

		}
		if pswUngleich == 0 {
			ok = false
			data.PswUngleich = false
		}

		if ok {

			u := model.User{
				Id:         userid,
				Type:       "User",
				Rev:        rev,
				Nutzername: username,
				Email:      email,
				Passwort:   passwortNeu,
			}
			u.UpdateUser()
		}
	}
}

// AddUser controller
func Add_User(w http.ResponseWriter, r *http.Request) {
	data.EmailFalse = false
	data.NameFalse = false
	data.CheckBoxFalse = false
	data.PswFalse = false

	nutzername := r.FormValue("nutzername")
	email := r.FormValue("email")
	passwort := r.FormValue("passwort")
	passwort2 := r.FormValue("passwort2")
	checkbox := r.FormValue("checkbox")

	if len(nutzername) != 0 && len(email) != 0 && len(passwort) != 0 {
		u := model.User{
			Type:       "User",
			Nutzername: nutzername,
			Email:      email,
			Passwort:   passwort,
		}

		namebool := model.CheckUserName(nutzername)
		emailbool := model.CheckEmail(email)
		var checkboxbool bool
		if strings.Compare(checkbox, "on") == 0 {
			checkboxbool = true
		} else {
			checkboxbool = false
		}
		var pswbool bool
		if strings.Compare(passwort, passwort2) != 0 {
			pswbool = false
		} else {
			pswbool = true
		}
		ok := true
		if !namebool {
			println("wrong name ")
			ok = false
			data.NameFalse = true

		}

		if !emailbool {
			println("wrong email ")
			ok = false
			data.EmailFalse = true

		}
		if !pswbool {
			println("wrong psw ")
			ok = false
			data.PswFalse = true
		}

		if !checkboxbool {
			println("wrong checkbox ")
			ok = false
			data.CheckBoxFalse = true
		}

		if ok {

			userID, _ := u.AddUser()

			session, _ := store.Get(r, "session")

			session.Values["userid"] = userID
			session.Values["authenticated"] = true //
			session.Save(r, w)
			data.Login = false

			http.Redirect(w, r, "/users/meinekarteien/0", 302)
		} else {
			temp := data

			tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/registration.tmpl", "template/login.tmpl")
			tmpl.ExecuteTemplate(w, "layout", temp)
		}

	}

}

//Login
func AuthenticateUser(w http.ResponseWriter, r *http.Request) {

	nutzername := r.FormValue("nutzername")
	passwort := r.FormValue("passwort")
	temp := data
	temp.Error = false
	// Authentication
	user, err := model.GetUserByUsername(nutzername)

	if err == nil {

		// decode base64 String to []byte
		passwordDB, _ := base64.StdEncoding.DecodeString(user.Passwort)

		err = bcrypt.CompareHashAndPassword(passwordDB, []byte(passwort))
		if err == nil {
			temp.Error = false
			data.Login = false
			temp.Login = false

			session, _ := store.Get(r, "session")

			// Set user as authenticated
			session.Values["authenticated"] = true
			session.Values["username"] = nutzername
			session.Values["userid"] = user.Id
			session.Save(r, w)

			http.Redirect(w, r, "/users/meinekarteien/0", http.StatusFound)
		} else {
			temp.Error = true

			tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/startseite.tmpl", "template/login.tmpl")
			tmpl.ExecuteTemplate(w, "layout", temp)

		}
	} else {
		temp.Error = true

		tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/startseite.tmpl", "template/login.tmpl")
		tmpl.ExecuteTemplate(w, "layout", temp)

	}

}

// Logout controller
func Logout(w http.ResponseWriter, r *http.Request) {

	session, _ := store.Get(r, "session")

	session.Values["authenticated"] = false
	session.Values["username"] = ""
	session.Values["userid"] = ""
	session.Save(r, w)
	data.Login = true
	tmpl, _ = template.ParseFiles("template/layout.tmpl", "template/startseite.tmpl", "template/login.tmpl")
	tmpl.ExecuteTemplate(w, "layout", data)
}

// Auth is an authentication handler
func Auth(h http.HandlerFunc) http.HandlerFunc {

	return func(w http.ResponseWriter, r *http.Request) {
		session, _ := store.Get(r, "session")

		// Check if user is authenticated
		if auth, ok := session.Values["authenticated"].(bool); !ok || !auth {

			http.Redirect(w, r, "/", http.StatusFound)
		} else {
			h(w, r)
		}
	}
}

func DeleteUserProfil(w http.ResponseWriter, r *http.Request) {

	session, _ := store.Get(r, "session")
	userid := session.Values["userid"].(string)
	user := model.GetUserById(userid)

	var lns []model.User_leraning_history
	kasten, _ := model.GetKaestenByUserId(userid)

	for i := 0; i < len(kasten); i++ {
		ln := model.GetLernHistoryByKastenid(kasten[i].Id)

		for j := 0; j < len(ln); j++ {

			lns = append(lns, ln[j])
		}
	}

	for i := 0; i < len(lns); i++ {
		lns[i].DeleteLernenHistory()
	}

	for i := 0; i < len(kasten); i++ {
		kasten[i].DeleteKasten()
	}

	user.DeleteUser()

}
