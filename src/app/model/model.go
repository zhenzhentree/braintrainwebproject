package model

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"strconv"

	couchdb "github.com/leesper/couchdb-golang"
)

//// User learning history structure
type User_leraning_history struct {
	Id          string `json:"_id"`
	Rev         string `json:"_rev"`
	Userid      string `json:"userid"`
	Kastenid    string `json:"kastenid"`
	Karteid     string `json:"karteid"`
	Kartelevel  string `json:"kartelevel"`  //"karte level"
	Kastenlevel string `json:"kastenlevel"` //kasten level
	couchdb.Document
}

// Karte data structure
type Karte struct {
	Id       string `json:"_id"`
	Rev      string `json:"_rev"`
	Type     string `json:"type"`
	Level    string `json:"level"`
	Title    string `json:"title"`
	Frage    string `json:"frage"`
	Antwort  string `json:"antwort"`
	Kastenid string `json:"kastenid"`
	couchdb.Document
}

// Karte data structure
type Kasten struct {
	Id           string `json:"_id"`
	Rev          string `json:"_rev"`
	Type         string `json:"type"`
	Title        string `json:"title"`
	Category     string `json:"category"`
	Obercategory string `json:"obercategory"`
	Beschreibung string `json:"beschreibung"`
	Sichtbarkeit string `json:"sichtbarkeit"`
	Userid       string `json:"userid"`
	Level        string `json:"level"`
	couchdb.Document
}

// Db handle
var btDBkarte *couchdb.Database
var BtDBkasten *couchdb.Database

var btDBLernenHistory *couchdb.Database

func init() {
	var err error
	btDBkarte, err = couchdb.NewDatabase("http://couchdb:5984/karte")
	BtDBkasten, err = couchdb.NewDatabase("http://couchdb:5984/kasten")

	btDBLernenHistory, err = couchdb.NewDatabase("http://couchdb:5984/user_learning_history")
	if err != nil {
		panic(err)
	}
}

// Add Kasten to DB
func (kst Kasten) AddKasten() (string, error) {
	// Convert Kasten struct to map[string]interface as required by Save() method
	kasten := Kasten2Map(kst)

	// Delete _id and _rev from map, otherwise DB access will be denied (unauthorized)
	delete(kasten, "_id")
	delete(kasten, "_rev")

	// AddKarte karte to DB
	id, _, err := BtDBkasten.Save(kasten, nil)

	if err != nil {
		fmt.Printf("[AddKasten] error: %s", err)
	}

	return id, err
}

func (kst Kasten) UpdateKasten() error {
	// Convert Kasten struct to map[string]interface as required by Save() method
	kasten := Kasten2Map(kst)

	// AddKarte karte to DB

	err := BtDBkasten.Set(kst.Id, kasten) // Set creates or updates a document with the specified ID.

	if err != nil {
		fmt.Printf("[UpdateKasten] error: %s", err)
	}

	return err
}

// Add Karte to DB
func (k Karte) AddKarte() error {
	// Convert Karte struct to map[string]interface as required by Save() method
	karte := Karte2Map(k)

	// Delete _id and _rev from map, otherwise DB access will be denied (unauthorized)
	delete(karte, "_id")
	delete(karte, "_rev")

	// AddKarte karte to DB
	_, _, err := btDBkarte.Save(karte, nil)

	if err != nil {
		fmt.Printf("[AddKarte] error: %s", err)
	}

	return err
}
func (k Karte) UpdateKarte() error {
	// Convert Karte struct to map[string]interface as required by Save() method
	karte := Karte2Map(k)

	// AddKarte karte to DB
	_, _, err := btDBkarte.Save(karte, nil)

	if err != nil {
		fmt.Printf("[UpdateKarte] error: %s", err)
	}

	return err
}

//Get Karte with the provided id from DB  //lernen
func GetKastenByKastenId(id string) (Kasten, error) {
	k, err := BtDBkasten.Get(id, nil)
	if err != nil {
		return Kasten{}, err
	}
	kasten := Kasten{
		Id:           k["_id"].(string),
		Rev:          k["_rev"].(string),
		Type:         k["type"].(string),
		Title:        k["title"].(string),
		Category:     k["category"].(string),
		Obercategory: k["obercategory"].(string),
		Beschreibung: k["beschreibung"].(string),
		Sichtbarkeit: k["sichtbarkeit"].(string),
		Userid:       k["userid"].(string),
		Level:        k["level"].(string),
	}
	return kasten, nil
}

//lernen
func GetUserById(userid string) User {
	u, _ := btDBuser.Get(userid, nil)

	user, _ := map2User(u)

	return user
}

// //lernen
func GetKartenCountUndKartenByFach(fach int, kastenid string) (int, []Karte) { //fach=karte level
	fachString := strconv.Itoa(fach)
	k, _ := btDBkarte.QueryJSON(`
	{
		"selector": {
			 "level": "` + fachString + `",
			 "kastenid": "` + kastenid + `"
		}
	 }`)
	karten := make([]Karte, len(k))
	for i := 0; i < len(k); i++ {
		karten[i], _ = map2Karte(k[i])
	}

	return len(k), karten

}

func GetKartenCountByKarteLevelLernHistory(kartelevel string, userid string, kastenid string) int { //fach=karte level
	ln, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			 "userid": "` + userid + `",
			 "kastenid": "` + kastenid + `",
			 "kartelevel": "` + kartelevel + `"
		}
	 }`)

	return len(ln)
}

// Get Karte with the provided id from DB
func GetKarte(karteid string) (Karte, error) {

	k, err := btDBkarte.Get(karteid, nil)

	karte, _ := map2Karte(k)

	return karte, err

}

//func ReturnGelerntPublicKaesten(id string) ([]Kasten, error) {}

func GetKaestenByUserId(id string) ([]Kasten, error) {
	k, err := BtDBkasten.QueryJSON(`
	{
		"selector": {
			 "userid": "` + id + `"
					
			 
		}
	 }`)
	if err != nil {
		return nil, err
	} else {
		kaesten := make([]Kasten, len(k))
		for i := 0; i < len(k); i++ {

			kaesten[i] = Kasten{
				Id:           k[i]["_id"].(string),
				Rev:          k[i]["_rev"].(string),
				Type:         k[i]["type"].(string),
				Title:        k[i]["title"].(string),
				Category:     k[i]["category"].(string),
				Obercategory: k[i]["obercategory"].(string),
				Beschreibung: k[i]["beschreibung"].(string),
				Sichtbarkeit: k[i]["sichtbarkeit"].(string),
				Userid:       k[i]["userid"].(string),
				Level:        k[i]["level"].(string),
			}

		}
		return kaesten, nil
	}
}

// Get all Karten from DB
func GetAllKartenUndKartenCount() ([]Karte, int) {
	k, _ := btDBkarte.QueryJSON(`
	{
		"selector": {
			 "type": "karte"	 
		}
	 }`)
	kartencount := len(k)
	karten := make([]Karte, len(k))
	for i := 0; i < len(k); i++ {

		karten[i] = Karte{
			Id:       k[i]["_id"].(string),
			Rev:      k[i]["_rev"].(string),
			Type:     k[i]["type"].(string),
			Title:    k[i]["title"].(string),
			Frage:    k[i]["frage"].(string),
			Antwort:  k[i]["antwort"].(string),
			Kastenid: k[i]["kastenid"].(string),
			Level:    k[i]["level"].(string),
		}
	}
	return karten, kartencount
}

//startseite
func GetAllKaestenCount() int {
	k, _ := BtDBkasten.QueryJSON(`
	{
		"selector": {
			 "type": "Kasten"	 
		}
	 }`)
	kartencount := len(k)
	return kartencount

}

//func GetGelernteKastenVonAnderenUser(kastenid string) int {}

func GetAllKartenByKastenID(id string) ([]Karte, int) {
	k, _ := btDBkarte.QueryJSON(`
	{
		"selector": {
			 "kastenid": "` + id + `"		 
		}
	 }`)
	kartencount := len(k)
	karten := make([]Karte, kartencount)
	for i := 0; i < kartencount; i++ {

		karten[i] = Karte{
			Id:       k[i]["_id"].(string),
			Rev:      k[i]["_rev"].(string),
			Type:     k[i]["type"].(string),
			Title:    k[i]["title"].(string),
			Frage:    k[i]["frage"].(string),
			Antwort:  k[i]["antwort"].(string),
			Kastenid: k[i]["kastenid"].(string),
			Level:    k[i]["level"].(string),
		}

	}

	return karten, kartencount
}

func GetPublicKastenCount() int {

	allPiublicKasten, _ := BtDBkasten.QueryJSON(`
	{
		"selector": {
			 "type": "Kasten",		 
			 "sichtbarkeit" : "öffentlich"
		}
	 }`)

	count := len(allPiublicKasten)

	return count

}

func GetCount(tabelle string, dbhandler *couchdb.Database) int {

	allinstances, _ := dbhandler.QueryJSON(`
	{
		"selector": {
			 "type": {
					"$eq": "` + tabelle + `"
			 }
		}
	 }`)
	var count int
	count = len(allinstances)

	return count
}

func GetUserCount() int {
	user, _ := btDBuser.QueryJSON(`
	{
		"selector": {
			 "type": "User"		 
		}
	 }`)
	count := len(user)

	return count

}

func GetMeinePrivateKastencount(userid string) int {
	allMeineKasten, _ := BtDBkasten.QueryJSON(`
	{
		"selector": {
			 "type": "Kasten",		 
			 "sichtbarkeit" : "private",
			 "userid":  "` + userid + `"
		}
	 }`)
	var count int
	count = len(allMeineKasten)
	return count
}

func GetMeineKastencount(userid string) int {
	allMeineKasten, _ := BtDBkasten.QueryJSON(`
	{
		"selector": {
			 "type": "Kasten",		 
			 "userid":  "` + userid + `"
		}
	 }`)

	count := len(allMeineKasten)

	return count
}

//learninghistory
func GetAnderenKartenCountsByUserId(userid string) []int {
	k, _ := btDBLernenHistory.QueryJSON(`
{
	"selector": {		 
		 "userid":  "` + userid + `"
	}
 }`)

	kartencounts := make([]int, len(k))
	for i := 0; i < len(k); i++ {
		kastenid := k[i]["kastenid"].(string)
		kartencounts[i] = GetKartenCountById(kastenid)

	}
	return kartencounts
}

//meinekarteikasten
func GetKartenCountsByUserId(userid string) []int {
	k, _ := BtDBkasten.QueryJSON(`
{
	"selector": {
		 "type": "Kasten",		 
		 "userid":  "` + userid + `"
	}
 }`)

	kartencounts := make([]int, len(k))
	for i := 0; i < len(k); i++ {
		kastenid := k[i]["_id"].(string)
		kartencounts[i] = GetKartenCountById(kastenid)

	}
	return kartencounts
}

func GetKartenCountById(kastenid string) int {
	karten, _ := btDBkarte.QueryJSON(`
	{
		"selector": {
			 "type": "karte",		 
			 "kastenid" : "` + kastenid + `"
		}
	 }`)
	count := len(karten)

	return count

}

func ReturnOberCategoryUndCategoryCount() (map[string]string, int) {

	obercat, _ := BtDBkasten.QueryJSON(`{
		"fields": [
		   "obercategory"
		],
		"selector": {
		   "type": "Kasten"
		}
	 }`)

	obercategory := make([]string, len(obercat))

	//to make a set by tricking the key
	set := make(map[string]string)
	for i := 0; i < len(obercat); i++ {

		temp := obercat[i]["obercategory"].(string)
		set[temp] = temp
		obercategory[i] = set[temp]

	}
	count := len(set)

	return set, count
}

func ReturnKastenByOberCate(obercate string) []Kasten {

	selector := `{
		"selector": {
		   "type": "Kasten",
		   "sichtbarkeit": "öffentlich",
		   "obercategory": "` + obercate + `"
		   
		}
	 }`

	k, _ := BtDBkasten.QueryJSON(selector)

	kasten := make([]Kasten, len(k))

	for i := 0; i < len(k); i++ {

		kasten[i] = Kasten{
			Id:           k[i]["_id"].(string),
			Rev:          k[i]["_rev"].(string),
			Type:         k[i]["type"].(string),
			Title:        k[i]["title"].(string),
			Obercategory: k[i]["obercategory"].(string),
			Category:     k[i]["category"].(string),
			Userid:       k[i]["userid"].(string),
			Beschreibung: k[i]["beschreibung"].(string),
			Level:        k[i]["level"].(string),
		}

	}

	return kasten

}

func ErstellerIdEinesKastens(kastenid string) {

}

//meinekarteikasten //gelernte von anderen
func GetUnrepeatedKastenVonAnderenByUserId(userid string) []Kasten { //GetGelerntKaestenVonAnderenByUserId

	kstids, _ := btDBLernenHistory.QueryJSON(`{
		"fields": [
		   "kastenid"
		],
		"selector": {	  
		   "userid": "` + userid + `"
		}
	 }`)

	var kastenids []string
	var kaesten []Kasten
	//to make a set by tricking the key
	set := make(map[string]string)
	for i := 0; i < len(kstids); i++ {

		temp := kstids[i]["kastenid"].(string)
		set[temp] = temp
	}

	for key, _ := range set {
		kastenids = append(kastenids, key)
	}
	for i := 0; i < len(kastenids); i++ {
		kst, _ := GetKastenByKastenId(kastenids[i])
		if kst.Userid != userid {
			kaesten = append(kaesten, kst)
		}
	}

	return kaesten
}

// Delete Kasten with the provided id from DB
func (k Kasten) DeleteKasten() error {
	karten, _ := GetAllKartenByKastenID(k.Id)
	for i := 0; i < len(karten); i++ {
		karten[i].DeleteKarte()
	}
	err := BtDBkasten.Delete(k.Id)
	if err != nil {
		fmt.Printf("[DeleteKasten] error: %s", err)
	}

	//delete auch entsprechende LernHistoryVonAnderen
	lerns := GetLernHistorysByUserKastenId(k.Id, k.Userid)
	for i := 0; i < len(lerns); i++ {
		lerns[i].DeleteLernenHistory()
	}

	return err
}
func GetLernHistorysByUserKastenId(userid string, kastenid string) []User_leraning_history {
	lerns, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			 "userid":  "` + userid + `",	 
			 "kastenid" : "` + kastenid + `"
		}
	 }`)
	lns := make([]User_leraning_history, len(lerns))
	for i := 0; i < len(lns); i++ {
		lns[i], _ = map2Lernen(lerns[i])
	}
	return lns
}

func (k Karte) DeleteKarte() error {

	err := btDBkarte.Delete(k.Id)
	if err != nil {
		fmt.Printf("[DeleteKarte] error: %s", err)
	}

	return err
}

// ---------------------------------------------------------------------------
// Internal helper functions
// ---------------------------------------------------------------------------

// Convert from struct to map[string]interface{} as required by Set() method
func Karte2Map(k Karte) map[string]interface{} {
	var doc map[string]interface{}
	tJSON, _ := json.Marshal(k) //return json encoding
	json.Unmarshal(tJSON, &doc)

	return doc
}

// Convert from struct to map[string]interface{} as required by Set() method
func Kasten2Map(kst Kasten) map[string]interface{} {
	var doc map[string]interface{}
	tJSON, _ := json.Marshal(kst) //return json encoding
	json.Unmarshal(tJSON, &doc)

	return doc
}

func User2Map(u User) map[string]interface{} {
	var doc map[string]interface{}
	tJSON, _ := json.Marshal(u) //return json encoding
	json.Unmarshal(tJSON, &doc)

	return doc
}
func lernen2Map(ln User_leraning_history) map[string]interface{} {
	var doc map[string]interface{}
	tJSON, _ := json.Marshal(ln) //return json encoding
	json.Unmarshal(tJSON, &doc)

	return doc
}
func map2Lernen(ln map[string]interface{}) (l User_leraning_history, err error) {
	uJSON, err := json.Marshal(ln)
	json.Unmarshal(uJSON, &l)

	return l, err
}

// Convert from map[string]interface{} to User struct as required by golang-couchdb methods
func map2Kasten(kasten map[string]interface{}) (k Kasten, err error) {
	uJSON, err := json.Marshal(kasten)
	json.Unmarshal(uJSON, &k)

	return k, err
}

func map2Karte(karte map[string]interface{}) (k Karte, err error) {
	uJSON, err := json.Marshal(karte)
	json.Unmarshal(uJSON, &k)

	return k, err
}

func FortschrittBerechnen(kastenid string) (prozent int) {
	sumeZaehler := 0
	for fachnr := 0; fachnr < 5; fachnr++ {
		count, _ := GetKartenCountUndKartenByFach(fachnr, kastenid)
		sumeZaehler = sumeZaehler + count*fachnr

	}
	alleKartenCount := GetKartenCountById(kastenid)

	if alleKartenCount == 0 {
		return 0
	}
	progress := (sumeZaehler * 100) / (4 * alleKartenCount)
	if progress >= 100 {
		return 100
	}

	return progress
}

func Filter(cat string) []Kasten { // "obercategory": "` + obercat + `",
	k, _ := BtDBkasten.QueryJSON(`
	{
		"selector": {
			"sichtbarkeit": "öffentlich",
			 "category": "` + cat + `"
		}
	 }`)
	kaesten := make([]Kasten, len(k))
	for i := 0; i < len(kaesten); i++ {
		kaesten[i], _ = map2Kasten(k[i])
	}
	return kaesten
}
func FilterKaestenVonAnderen(userid string, cat string) []Kasten {

	kaesten := GetUnrepeatedKastenVonAnderenByUserId(userid)

	var kst []Kasten
	for i := 0; i < len(kaesten); i++ {
		if kaesten[i].Category == cat {
			kst = append(kst, kaesten[i])
		}
	}
	return kst

}

func FilterPrivateKaesten(userid string, cat string) []Kasten {
	k, _ := BtDBkasten.QueryJSON(`
	{
		"selector": {
			"userid": "` + userid + `",
			
			 "category": "` + cat + `"
		}
	 }`)
	kaesten := make([]Kasten, len(k))
	for i := 0; i < len(kaesten); i++ {
		kaesten[i], _ = map2Kasten(k[i])
	}
	return kaesten

}

func KarteKastenLevelUp(kastenid string, karteid string) (fertigGelert bool, progress string) {
	fertigGelert = false
	karte, _ := GetKarte(karteid)
	kartelevel, _ := strconv.Atoi(karte.Level)
	if kartelevel < 4 {
		kartelevel++

		karte.Level = strconv.Itoa(kartelevel)

		karte.Id = karteid

	} else {
		fertigGelert = true
		return fertigGelert, strconv.Itoa(FortschrittBerechnen(kastenid)) //dann nicht mehr level up
	}
	karte.UpdateKarte()
	kasten, _ := GetKastenByKastenId(kastenid)
	kasten.Level = strconv.Itoa(FortschrittBerechnen(kastenid))

	kasten.UpdateKasten()

	return fertigGelert, kasten.Level
}
func GetLernHistorysByExactIds(userid string, kastenid string, karteid string) User_leraning_history {
	ln, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			"userid": "` + userid + `",		
			 "kastenid": "` + kastenid + `",
			 "karteid": "` + karteid + `"
		}
	 }`)
	if len(ln) != 0 {
		l, _ := map2Lernen(ln[0])
		return l
	} else {
		return User_leraning_history{}
	}

}
func LevelUp(userid string, kastenid string, karteid string) (fertigGelernt bool, progress string, lernlevel string) {
	ln := GetLernHistorysByExactIds(userid, kastenid, karteid)

	fertigGelernt = false
	level, _ := strconv.Atoi(ln.Kartelevel)
	if level < 4 {
		level++

		ln.Kartelevel = strconv.Itoa(level)

	} else {
		fertigGelernt = true
		ln.UpdateLernenHistory()
		return fertigGelernt, strconv.Itoa(Fortschritte_Berechnen(kastenid, userid)), strconv.Itoa(level) //dann nicht mehr level up
	}
	ln.UpdateLernenHistory()
	ln = GetLernenById(ln.Id)
	ln.Kastenlevel = strconv.Itoa(Fortschritte_Berechnen(kastenid, userid))
	ln.UpdateLernenHistory()
	return fertigGelernt, ln.Kastenlevel, strconv.Itoa(level)
}

func LevelDown(userid string, kastenid string, karteid string) (progress string) {

	ln := GetLernHistorysByExactIds(userid, kastenid, karteid)

	ln.Kartelevel = "0"
	ln.UpdateLernenHistory()  //!
	ln = GetLernenById(ln.Id) //um die rev zu neu holen sont die 2. update functioniert nichts
	ln.Kastenlevel = strconv.Itoa(Fortschritte_Berechnen(kastenid, userid))
	ln.UpdateLernenHistory()
	return ln.Kastenlevel
}
func Fortschritte_Berechnen(kastenid string, userid string) int {
	sumeZaehler := 0
	for fachnr := 0; fachnr < 5; fachnr++ {
		count, _ := GetCountUndLernHistoryByFach(userid, kastenid, fachnr) //es ist hier angenommen dass der User den Kartei von anderen nicht bearbeiten darf
		sumeZaehler = sumeZaehler + count*fachnr
	}
	alleKartenCount := GetKartenCountById(kastenid)

	if alleKartenCount == 0 {
		return 0
	}
	progress := sumeZaehler * 100 / (4 * alleKartenCount)
	if progress >= 100 {
		return 100
	}
	return progress
}
func GetKartenCountByFachForLernHistory(fach int, kastenid string, userid string) int {
	fachString := strconv.Itoa(fach)
	ln, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			 "userid": "` + userid + `",
			 "kastenid": "` + kastenid + `",
			 "kartelevel": "` + fachString + `"
		}
	 }`)

	return len(ln)

}

func KarteKastenLevelDown(kastenid string, karteid string) string {
	karte, _ := GetKarte(karteid)

	karte.Level = "0"
	karte.UpdateKarte()
	kasten, _ := GetKastenByKastenId(kastenid)
	kasten.Level = strconv.Itoa(FortschrittBerechnen(kastenid))
	kasten.UpdateKasten()

	return kasten.Level

}

func ZufallsFach() int {
	r := rand.Intn(15) //[0,15)]

	var f int

	if r == 0 {
		f = 4
	}
	if r == 1 || r == 2 {
		f = 3
	}
	if r <= 5 && r >= 2 {

		f = 2
	}
	if r <= 9 && r >= 6 {
		f = 1
	}
	if r <= 14 && r >= 10 {
		f = 0
	}

	return f
}
func NaechsteKarte(userid string, kastenid string) Karte {
	var k Karte
	var f int
	_, kartencount := GetAllKartenByKastenID(kastenid)
	if kartencount != 0 { //check if the totam karten number is 0 to avoid a endless loop
		for k.Id == "" {
			f = ZufallsFach()
			k = ZufallsKarteAusFach(userid, kastenid, f)
		}
	}

	return k
}
func AlleKartenDerKarteiGelernt(userid string, kastenid string) bool {

	ln, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			"userid": "` + userid + `",
			"kastenid": "` + kastenid + `",
		   "kartelevel": {
			  "$lt": "4"
		   }
		}
	 }`)

	if len(ln) == 0 {
		return true
	} else {
		return false
	}
}
func ZufallsKarteAusFach(userid string, kastenid string, fach int) Karte {
	fachkartencount, lns := GetCountUndLernHistoryByFach(userid, kastenid, fach)
	if fachkartencount == 0 {

		return Karte{}
	}

	ran := rand.Intn(fachkartencount) //para must be >0
	k, _ := GetKarte(lns[ran].Karteid)
	return k
}

func KastenVonAnderenBool(userid string, kastenid string) bool {
	kasten, _ := GetKastenByKastenId(kastenid)
	if userid != kasten.Userid {
		return true
	} else {
		return false
	}
}

func (lernen User_leraning_history) AddLernen() (id string, err error) {

	ln := lernen2Map(lernen)

	// Delete _id and _rev from map, otherwise DB access will be denied (unauthorized)
	delete(ln, "_id")
	delete(ln, "_rev")

	// AddKarte karte to DB
	id, _, err = btDBLernenHistory.Save(ln, nil)

	if err != nil {
		fmt.Printf("[AddLernen] error: %s", err)
	}

	return id, err

}

func GetLernenById(lernid string) (ln User_leraning_history) {
	l, _ := btDBLernenHistory.Get(lernid, nil)

	ln, _ = map2Lernen(l)

	return ln
}

func (ln User_leraning_history) UpdateLernenHistory() error {
	// Convert Karte struct to map[string]interface as required by Save() method
	lernen := lernen2Map(ln)
	// AddKarte karte to DB
	err := btDBLernenHistory.Set(ln.Id, lernen)

	if err != nil {
		fmt.Printf("[UpdateLernen] error: %s", err)
	}

	return err
}
func (ln User_leraning_history) DeleteLernenHistory() error {

	err := btDBLernenHistory.Delete(ln.Id)
	if err != nil {
		fmt.Printf("[DeleteLernen] error: %s", err)
	}

	return err
}

//check ob diese Karte für diese User schon ein Entry in couchdb gibt
func DocNotExistForKarte(userid string, karteid string) (bool, string) {

	ln, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			 "userid": "` + userid + `",
			 "karteid": "` + karteid + `"
		}
	 }`)
	if len(ln) == 0 {
		return true, ""
	}
	lern, _ := map2Lernen(ln[0])
	return false, lern.Id //falls ein doc existiert retrun die Id

}

func GetLernenHistoryByUserId(userid string) []User_leraning_history {
	ln, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			 "userid": "` + userid + `"			
		}
	 }`)
	lernen := make([]User_leraning_history, len(ln))
	for i := 0; i < len(ln); i++ {
		lernen[i], _ = map2Lernen(ln[i])

	}
	return lernen
}

func GetKastenlevelLernHistory(userid string, kastenid string) string {
	ln, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			 "userid": "` + userid + `",
			 "kastenid": "` + kastenid + `"		
		}
	 }`)
	if len(ln) != 0 {
		return ln[0]["kastenlevel"].(string)
	} else {
		return ""
	}

}

func GetCountUndLernHistoryByFach(userid string, kastenid string, fach int) (int, []User_leraning_history) {
	fachstring := strconv.Itoa(fach)
	ln, _ := btDBLernenHistory.QueryJSON(`
	{
		"selector": {
			 "userid": "` + userid + `",
			 "kastenid": "` + kastenid + `",
			 "kartelevel": "` + fachstring + `"	
		}
	 }`)

	lern := make([]User_leraning_history, len(ln))
	for i := 0; i < len(ln); i++ {
		lern[i], _ = map2Lernen(ln[i])
	}
	return len(ln), lern
}
