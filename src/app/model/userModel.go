package model

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"strings"

	couchdb "github.com/leesper/couchdb-golang"
	"golang.org/x/crypto/bcrypt"
)

// User data structure
type User struct {
	Id             string `json:"_id"`
	Rev            string `json:"_rev"`
	Type           string `json:"type"`
	Nutzername     string `json:"nutzername"`
	Email          string `json:"email"`
	Passwort       string `json:"passwort"`
	ProfilBildName string `json:"profilbild"` //unique name built with userid and mime
	couchdb.Document
}

var btDBuser *couchdb.Database

func init() {
	var err error

	btDBuser, err = couchdb.NewDatabase("http://couchdb:5984/user")

	if err != nil {
		panic(err)
	}
}

// Add User
func (user User) AddUser() (string, error) {

	// Check wether username already exists
	userInDB, err := GetUserByUsername(user.Nutzername)
	if err == nil && userInDB.Nutzername == user.Nutzername {

		return "", errors.New("username exists already")
	}

	// Hash password
	hashedPwd, err := bcrypt.GenerateFromPassword([]byte(user.Passwort), 14)
	b64HashedPwd := base64.StdEncoding.EncodeToString(hashedPwd)

	user.Passwort = b64HashedPwd
	user.Type = "User"

	// Convert Todo struct to map[string]interface as required by Save() method
	u, err := user2Map(user)

	// Delete _id and _rev from map, otherwise DB access will be denied (unauthorized)
	delete(u, "_id")
	delete(u, "_rev")

	// Add todo to DB
	id, _, err := btDBuser.Save(u, nil)

	if err != nil {
		fmt.Printf("[Add] error: %s", err)
	}

	return id, err
}

// GetUserByUsername retrieve User by username
func GetUserByUsername(username string) (user User, err error) {
	if username == "" {
		return User{}, errors.New("no username provided")
	}

	query := `
	{
		"selector": {
			 "type": "User",
			 "nutzername": "%s"
		}
	}`
	u, err := btDBuser.QueryJSON(fmt.Sprintf(query, username))
	if err != nil || len(u) != 1 {
		return User{}, err
	}

	user, err = map2User(u[0])
	if err != nil {
		return User{}, err
	}

	return user, nil
}

func CheckUserName(name string) bool {
	user, _ := btDBuser.QueryJSON(`
		{
			"selector": {
			   "nutzername": "` + name + `"
			}
		 }`)

	if len(user) == 0 {
		return true
	}
	return false

}
func CheckEmail(email string) bool {

	user, _ := btDBuser.QueryJSON(`
		{
			"selector": {
			   "email": "` + email + `"
			}
		 }`)

	if len(user) == 0 {
		return true
	}
	return false

}

// ---------------------------------------------------------------------------
// Internal helper functions
// ---------------------------------------------------------------------------

// Convert from User struct to map[string]interface{} as required by golang-couchdb methods
func user2Map(u User) (user map[string]interface{}, err error) {
	uJSON, err := json.Marshal(u)
	json.Unmarshal(uJSON, &user)

	return user, err
}

// Convert from map[string]interface{} to User struct as required by golang-couchdb methods
func map2User(user map[string]interface{}) (u User, err error) {
	uJSON, err := json.Marshal(user)
	json.Unmarshal(uJSON, &u)

	return u, err
}

func map2String(mp map[string]interface{}) (s string) {
	temp := strings.TrimPrefix(fmt.Sprintf("%s", mp), "map[email:")
	ret := strings.TrimSuffix(temp, "]")
	return ret
}

func CheckPasswort(userid string, psw string) bool {

	user := GetUserById(userid)
	//cant not directly compare.
	// decode base64 String to []byte
	passwordDB, _ := base64.StdEncoding.DecodeString(user.Passwort)
	err := bcrypt.CompareHashAndPassword(passwordDB, []byte(psw))

	if err != nil {
		return false
	}
	return true
}

func (user User) UpdateUser() error {

	// Hash password
	hashedPwd, err := bcrypt.GenerateFromPassword([]byte(user.Passwort), 14)
	b64HashedPwd := base64.StdEncoding.EncodeToString(hashedPwd)

	user.Passwort = b64HashedPwd
	user.Type = "User"
	//vergiss nicht auch Bildname mitzugeben dasmit nach den profilupdate das bild gezeigt werden kann
	user.ProfilBildName = GetProfilBildName(user.Id)
	// Convert Todo struct to map[string]interface as required by Save() method
	u, _ := user2Map(user)

	// AddKarte karte to DB
	err = btDBuser.Set(user.Id, u) // Set creates or updates a document with the specified ID.

	if err != nil {
		fmt.Printf("[UpdateUser] error: %s", err)
	}

	return err
}

func (user User) UpdateProfilbild() error {
	u, _ := user2Map(user)
	err := btDBuser.Set(user.Id, u) // Set creates or updates a document with the specified ID.

	if err != nil {
		fmt.Printf("[UpdateUser] error: %s", err)
	}

	return err
}

func GetKartenCountByUserID(userid string) int {
	kaesten, _ := GetKaestenByUserId(userid)
	kartencount := 0
	for i := 0; i < len(kaesten); i++ {
		kartencount += GetKartenCountById(kaesten[i].Id)
	}

	return kartencount
}

// Delete User with the provided id from DB
func (u User) DeleteUser() error {
	err := btDBuser.Delete(u.Id)
	return err
}

func UploadProfilBild(doc map[string]interface{}, content []byte, name, mimeType string) {

	btDBuser.PutAttachment(doc, content, name, mimeType)
}

func GetProfilBildName(userid string) string {
	bildName, _ := btDBuser.QueryJSON(`{
		"fields": [
		   "profilbild"
		],
		"selector": {
		    "_id": "` + userid + `"
		}
	 }`)
	if len(bildName) != 0 && bildName != nil {
		if bildName[0]["profilbild"] != nil {
			image := bildName[0]["profilbild"].(string)
			return image
		}
	}
	return ""
}

func GetLernHistoryByKastenid(kastenid string) []User_leraning_history {
	ln, _ := btDBLernenHistory.QueryJSON(`
{
	"selector": {
		 "kastenid": "` + kastenid + `"		 
	}
 }`)

	lern := make([]User_leraning_history, len(ln))
	for i := 0; i < len(ln); i++ {
		lern[i], _ = map2Lernen(ln[i])
	}
	return lern
}
